/* eslint-disable @typescript-eslint/camelcase */
module.exports = {
  publicPath: '/',
  runtimeCompiler: true,

  devServer: {
    https: false,
    port: 8080,
    public: '0.0.0.0:8080',
    disableHostCheck: true,
    overlay: {
      warnings: true,
      errors: true
    }
  },

  chainWebpack: (config) => {
    config.plugins.delete('prefetch');

    config.optimization.splitChunks({
      chunks: 'all'
    });
  }
};
