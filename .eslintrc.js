module.exports = {
  root: true,

  env: {
    node: true
  },

  parserOptions: {
    ecmaVersion: 2020
  },

  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
    '@vue/typescript/recommended'
  ],

  overrides: [
    {
      files: [
        'src/**/__tests__/*.{j,t}s?(x)',
        'src/**/tests/unit/**/*.spec.{j,t}s?(x)'
      ],
      env: {
        jest: true
      }
    }
  ],

  rules: {

    /* //////////////
    // TypeScript ///
    ////////////// */
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/ban-ts-ignore': 'off',

    /* //////////
    // Native ///
    ////////// */
    indent: 'off',
    'import/no-dynamic-require': 'off',
    'no-async-promise-executor': 'off',
    'class-methods-use-this': 'off',
    'no-constant-condition': 'off',
    'linebreak-style': 'off',
    'global-require': 'off',
    'default-case': 'off',

    'indent-legacy': [
      'error',
      2,
      { SwitchCase: 1 }
    ],

    'no-empty': [
      'error',
      { allowEmptyCatch: true }
    ],

    'comma-dangle': [
      'error',
      {
        objects: 'never',
        imports: 'never',
        exports: 'never',
        functions: 'never'
      }
    ],

    'no-return-assign': [
      'error',
      'except-parens'
    ],

    'no-param-reassign': [
      'error',
      { props: false }
    ],

    'object-curly-newline': [
      'error',
      {
        ImportDeclaration: 'never',
        ExportDeclaration: 'never'
      }
    ],

    'prefer-destructuring': [
      'error',
      {
        VariableDeclarator: {
          array: true,
          object: true
        },
        AssignmentExpression: {
          array: false,
          object: true
        }
      },
      { enforceForRenamedProperties: false }
    ],

    'lines-between-class-members': [
      'error',
      'always',
      { exceptAfterSingleLine: true }
    ],

    /* /////////////
    // Variables ///
    ///////////// */
    'no-alert': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',

    /* /////////
    // VueJS ///
    ///////// */
    'vue/v-on-style': 'error',
    'vue/html-quotes': 'error',
    'vue/v-bind-style': 'error',
    'vue/v-slot-style': 'error',
    'vue/html-end-tags': 'error',
    'vue/no-multi-spaces': 'error',
    'vue/prop-name-casing': 'error',
    'vue/html-self-closing': 'error',
    'vue/no-template-shadow': 'error',
    'vue/name-property-casing': 'error',
    'vue/attribute-hyphenation': 'error',
    'vue/no-unsupported-features': 'error',
    'vue/max-attributes-per-line': 'error',
    'vue/no-reserved-component-names': 'error',
    'vue/no-deprecated-slot-attribute': 'error',
    'vue/html-closing-bracket-newline': 'error',
    'vue/no-deprecated-scope-attribute': 'error',
    'vue/mustache-interpolation-spacing': 'error',
    'vue/no-deprecated-slot-scope-attribute': 'error',
    'vue/multiline-html-element-content-newline': 'error',
    'vue/no-spaces-around-equal-signs-in-attribute': 'error',

    'vue/this-in-template': 'warn',
    'vue/no-confusing-v-for-v-if': 'warn',

    'vue/singleline-html-element-content-newline': [
      'error',
      {
        ignoreWhenNoAttributes: true,
        ignoreWhenEmpty: true,
        ignores: [
          'textarea',
          'pre',
          'li',
          'h1',
          'h2',
          'h3',
          'h4',
          'h5',
          'h6',
          'p',
          'a',
          'i',
          'b',
          'em',
          'strong',
          'button',
          'span'
        ]
      }
    ],

    'vue/html-closing-bracket-spacing': [
      'error',
      {
        selfClosingTag: 'never',
        startTag: 'never',
        endTag: 'never'
      }
    ],

    'vue/component-tags-order': [
      'error',
      {
        order: ['template', 'script', 'style']
      }
    ]
  }
};
