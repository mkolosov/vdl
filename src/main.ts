import Vue from 'vue';
import App from '@/App.vue';

import '@/plugins';

import i18n from '@/plugins/i18n';
import vuex from '@/scripts/store';
import router from '@/scripts/router';
import IconStore from '@/scripts/IconStore';

Vue.config.performance = true;
Vue.config.productionTip = true;
window.timelapseStart = Date.now();

IconStore.init();

new Vue({
  i18n,
  router,
  store: vuex,
  render: (h) => h(App)
}).$mount('#app');
