/* eslint-disable */
import { load } from '@/plugins/i18n/helper';

export default {
  'testing-section': {
    'test': 'This is test',
    'longtext': load('en', 'test')
  }
};
