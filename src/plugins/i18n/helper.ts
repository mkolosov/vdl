/* eslint-disable import/prefer-default-export */
/* eslint-disable @typescript-eslint/no-var-requires */

export const load = (locale, path) => require(`raw-loader!../../assets/locales/${locale}/${path}.txt`).default;
