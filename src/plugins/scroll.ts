import Vue from 'vue';
import { OverlayScrollbarsComponent } from 'overlayscrollbars-vue';

Vue.component('Scrollbar', OverlayScrollbarsComponent);
