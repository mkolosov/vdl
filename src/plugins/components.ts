import Vue from 'vue';

Vue.component('BaseTabs', () => import('@/components/ui/BaseTabs.vue'));
Vue.component('BaseIcon', () => import('@/components/ui/BaseIcon.vue'));
Vue.component('BaseMenu', () => import('@/components/ui/BaseMenu.vue'));
Vue.component('BaseImage', () => import('@/components/ui/BaseImage.vue'));
Vue.component('BaseModal', () => import('@/components/ui/BaseModal.vue'));
Vue.component('BaseInput', () => import('@/components/ui/BaseInput.vue'));
Vue.component('BaseButton', () => import('@/components/ui/BaseButton.vue'));
Vue.component('BaseSelect', () => import('@/components/ui/BaseSelect.vue'));
Vue.component('BaseAsyncList', () => import('@/components/ui/BaseAsyncList.vue'));
Vue.component('BaseBreadcrumbs', () => import('@/components/ui/BaseBreadcrumbs.vue'));
Vue.component('BaseDataTable', () => import('@/components/ui/BaseDataTable.vue'));
Vue.component('BaseProgressBar', () => import('@/components/ui/BaseProgressBar.vue'));
