import Vue from 'vue';

Vue.directive('click-outside', {
  bind(el, { def, value }, { context }) {
    def.onDocumentClick = ({ target }) => {
      if (!context.$vClickOutsideActive) return;

      let isExistsIn = false;
      let parent = target;

      while (parent !== document.body) {
        if (parent === context.$el) {
          isExistsIn = true;
          break;
        }

        parent = parent.parentElement;
      }

      if (!isExistsIn && value instanceof Function) value();
    };

    def.onElementUpdated = (after) => {
      if (!after) context.$vClickOutsideActive = false;
      else setTimeout(() => (context.$vClickOutsideActive = true));
    };

    context.$on('update:click-outside', def.onElementUpdated);
    document.addEventListener('click', def.onDocumentClick);
  },

  unbind(el, { def }, { context }) {
    context.$off('update:click-outside', def.onElementUpdated);
    document.removeEventListener('click', def.onDocumentClick);
  }
} as any);
