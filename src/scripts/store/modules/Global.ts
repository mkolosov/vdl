import { VuexModule, Module, Mutation } from 'vuex-module-decorators';
import MobileDetect from 'mobile-detect';
import platform from 'platform';

export interface VDError {
  type?: VDErrorTypes;
  title?: string;
  msg: string;
}

export enum VDErrorTypes {
  WARNING,
  FAILED,
  SUCCESS
}

@Module({ namespaced: true })
export default class Global extends VuexModule {
  md = new MobileDetect(window.navigator.userAgent);

  mainTheme = 'default';
  error: VDError = null;

  // --------------------------------- //
  // ------------ Getters ------------ //
  // --------------------------------- //

  get isMobile() {
    return this.md.mobile();
  }

  get isIOS() {
    return platform.name === 'Safari' && platform.os.family === 'iOS';
  }

  get isChromium() {
    const supporters = ['Chrome', 'Chrome Mobile', 'Microsoft Edge'];
    return supporters.indexOf(platform.name) >= 0;
  }

  // --------------------------------- //
  // ------------ Actions ------------ //
  // --------------------------------- //

  // TODO

  // ----------------------------------- //
  // ------------ Mutations ------------ //
  // ----------------------------------- //

  @Mutation setError(data) {
    this.error = data;
  }
}
