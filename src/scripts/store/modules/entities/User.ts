import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators';

@Module({ namespaced: true })
export default class User extends VuexModule {
  isAuth = false;

  // -------------------------------- //
  // ------------ Getters ----------- //
  // -------------------------------- //

  // TODO

  // -------------------------------- //
  // ------------ Actions ----------- //
  // -------------------------------- //

  @Action init() {
    return this.context.commit('setAuth', true);
  }

  @Action signOut() {
    return this.context.commit('setAuth', false);
  }

  // ----------------------------------- //
  // ------------ Mutations ------------ //
  // ----------------------------------- //

  /**
   * Set authorization state
   */
  @Mutation setAuth(flag) {
    this.isAuth = flag;

    if (!flag) {
      window.location.href = '/';
    }
  }
}
