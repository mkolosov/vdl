import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators';
import API from '@/scripts/API';

@Module({ namespaced: true })
export default class Firms extends VuexModule {
  list = [];

  // -------------------------------- //
  // ------------ Getters ----------- //
  // -------------------------------- //

  get assignedList() {
    return this.list;
  }

  // -------------------------------- //
  // ------------ Actions ----------- //
  // -------------------------------- //

  @Action init({ force = false } = {}) {
    if (!force && this.list.length) return null;
    this.context.commit('flush');

    return API.get('firms').execute().then((res) => {
      if (res.ok && res.msg.data) {
        this.context.commit('save', res.msg.data);
      }
    });
  }

  // ----------------------------------- //
  // ------------ Mutations ------------ //
  // ----------------------------------- //

  @Mutation flush() {
    this.list = [];
  }

  @Mutation save(data) {
    this.list = data;
  }

  @Mutation add(data) {
    this.list.push(data);
  }

  @Mutation delete(id) {
    const index = this.list.findIndex((item) => item.id === id);
    this.list.splice(index, 1);
  }
}
