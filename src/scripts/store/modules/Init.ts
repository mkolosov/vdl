import { VuexModule, Module, Action } from 'vuex-module-decorators';

import vuex from '@/scripts/store';

@Module({ namespaced: true })
export default class Init extends VuexModule {
  // --------------------------------- //
  // ------------ Getters ------------ //
  // --------------------------------- //

  // TODO

  // --------------------------------- //
  // ------------ Actions ------------ //
  // --------------------------------- //

  @Action
  async preInit() {
    await vuex.dispatch('User/init');
  }

  // ----------------------------------- //
  // ------------ Mutations ------------ //
  // ----------------------------------- //

  // TODO
}
