import { VuexModule, Module, Action, Mutation } from 'vuex-module-decorators';
import Utils from '@/scripts/utils';

export enum States {
  PENDING,
  FAILED,
  LOADED
}

@Module({ namespaced: true })
export default class Vendors extends VuexModule {
  callbacks = new Map<string, Array<() => void>>();

  libs = {};
  states = {};
  styles = [];

  // -------------------------------- //
  // ------------ Getters ----------- //
  // -------------------------------- //

  // TODO

  // -------------------------------- //
  // ------------ Actions ----------- //
  // -------------------------------- //

  /**
   * Load the script and save it in memory
   * @param loader Loader function
   * @param key Script ID
   */
  @Action
  async loadModule({ loader, key } = {} as any) {
    if (key && loader && Object.keys(this.libs).indexOf(key) === -1) {
      const timelapseStart = Date.now();

      Utils.Console.timelapseModule(`Loading "${key}"...`);
      this.context.commit('saveModule', { key });

      const vendor = await loader();
      const timeTook = Date.now() - timelapseStart;

      Utils.Console.timelapseModule(`Loaded "${key}" in`, timeTook, 'ms');
      this.context.commit('saveModule', { key, vendor });

      if (this.callbacks.has(key)) {
        this.callbacks.get(key).forEach((callback) => callback());
      }
    }
  }

  /**
   * Get the loaded script when ready
   * @param key Script ID
   */
  @Action getModule(key) {
    return new Promise((resolve) => {
      if (this.states[key] === States.LOADED) {
        resolve(this.libs[key]);
        return;
      }

      if (!this.callbacks.has(key)) this.callbacks.set(key, []);
      this.callbacks.get(key).push(() => resolve(this.libs[key]));
    });
  }

  /**
   * Load style file, if it haven’t done so early
   * @param path Path of css-file (aka ID)
   * @param force If false - shouldn't work
   */
  @Action
  async loadStyle({ path, force = process.env.VUE_APP_IMPORT_CSS_PER_FILE } = {} as any) {
    const allIsImported = this.styles.indexOf('entries/all') !== -1;

    if (force && !allIsImported && this.styles.indexOf(path) === -1) {
      Utils.Console.create()
        .addTags('APP', 'CSS')
        .end(path);

      this.context.commit('saveStyle', { path });

      await import(
        /* webpackPreload: true */
        `@/assets/stylesheets/scss/${path}.scss`
      );
    }
  }

  /**
   * Load style entry, if it haven’t done so early
   * @param name Name of style entry
   * @param force If false - shouldn't work
   */
  @Action loadStyleEntry({ name, force = true } = {} as any) {
    return this.context.dispatch('loadStyle', {
      path: `entries/${name}`,
      force
    });
  }

  // ----------------------------------- //
  // ------------ Mutations ------------ //
  // ----------------------------------- //

  /**
   * Save script to memory
   * @param key Script ID
   * @param vendor Script object
   */
  @Mutation saveModule({ key, vendor = null }) {
    this.libs[key] = vendor;
    this.states[key] = vendor ? States.LOADED : States.PENDING;
  }

  /**
   * Set state of css-file to loaded
   * @param path Path of css-file (aka ID)
   */
  @Mutation saveStyle({ path }) {
    this.styles.push(path);
  }
}
