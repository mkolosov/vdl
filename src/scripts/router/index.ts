import Vue from 'vue';
import Router from 'vue-router';

import vuex from '@/scripts/store';
import Utils from '@/scripts/utils';
import { routes } from './routes';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: routes.map((route) => ({
    ...route,
    meta: {
      ...route.meta,
      title: (route.meta || {}).title
    },
    component: () => import(`@/pages/${route.component}.vue`)
  })) as any
});

router.beforeEach(async (to, fr, next) => {
  let nextPage = null;

  // Call preInit method from vuex for Application asynchronously or not
  const { isPreInited } = (vuex.state as any).Init || {};
  if ((!to.meta || !to.meta.vuexPreventPreInit) && !isPreInited) {
    if (to.meta && to.meta.vuexAsyncPreInit) {
      vuex.dispatch('Init/preInit', to.meta.vuexPreInitConfig);
    } else await vuex.dispatch('Init/preInit', (to.meta || {}).vuexPreInitConfig);
  }

  // Call preInit method from vuex for current route
  if (to.meta && to.meta.vuexInit) {
    await vuex.dispatch(to.meta.vuexInit, {
      route: to,
      data: to.meta.vuexData
    });
  }

  // Move to auth page if need auth-state,
  // or to home, if needn't,
  // or just skip if auth-state is not matter
  if (to.meta && typeof to.meta.auth === 'boolean') {
    const { isAuth } = vuex.state[to.meta.authModule || 'User'];
    const isNeedAuth = to.meta.auth && !isAuth;
    const isAuthed = to.meta.auth === false && isAuth;

    routes.forEach((route) => {
      if (nextPage) return;

      const isSameGroup = (route.meta || {}).group === to.meta.group;
      const isAuthInGroup = isSameGroup && route.meta.auth === false;
      const isHomeInGroup = isSameGroup && route.meta.isHome;

      if ((isNeedAuth && isAuthInGroup) || (isAuthed && isHomeInGroup)) nextPage = route.name;
    });
  }

  // Load route style entry or all app styles
  if (!nextPage) {
    if ((to.meta || {}).cssEntry) {
      await vuex.dispatch('Vendors/loadStyleEntry', { name: to.meta.cssEntry });
    } else {
      await vuex.dispatch('Vendors/loadStyleEntry', { name: 'all' });
    }

    if ((to.meta || {}).cssUsedPaths) {
      // If loaded entries then saving what styles that used
      to.meta.cssUsedPaths.forEach((path) => {
        vuex.commit('Vendors/saveStyle', { path });
      });
    }
  } else if (!nextPage) {
    await vuex.dispatch('Vendors/loadStyleEntry', { name: 'all', force: true });
  }

  Utils.Console.create()
    .addTags('APP', 'Router')
    .end(nextPage
      ? `Redirecting: ${to.name} -> ${nextPage}`
      : `Loading route: ${to.name}`);

  return next(nextPage ? { name: nextPage } : undefined);
});

router.afterEach((route) => {
  Utils.Console.create()
    .addTags('APP', 'Router')
    .end('Route loaded');

  Utils.Document.setDocumentTitle(route);
  Utils.Document.setLoading(false);
});

export default router;
