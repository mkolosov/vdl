export interface Route {
  // URL-path for page
  path?: string;

  // Name of route
  name?: string;

  // Redirect settings
  redirect?: any;

  // Path to page relative from src/pages
  component?: string;

  meta?: {
    // Is page need to be keeping in memory (without re-render, when route is changed)
    keep?: boolean;
    // Class of #app
    class?: string;
    // Group of route (for detecting auth- and home-page for auto-redirect). By default is "main"
    group?: string;
    // Title of document on this route
    title?: string;

    // Is page needs auth
    auth?: boolean;
    // Is page is home in that's group
    isHome?: boolean;

    // /////////////
    // vuex names //
    // /////////////

    // What method from store must be called before enter to the route
    vuexInit?: string;
    // What module has "isAuth" variable for auth-detection?
    // (checking if "auth" is true. "User" module by default)
    authModule?: string;
    // What data passing to the vuexInit method
    vuexData?: any;

    vuexPreInitConfig?: any;
    // Is route must prevent Init/preInit
    vuexPreventPreInit?: boolean;
    // Is route must awaiting Init/preInit? (not work if prev. param is true). By default is true
    vuexAsyncPreInit?: boolean;

    // ////////////////////
    // Visual parameters //
    // ////////////////////

    // Name of styles for route
    cssEntry?: string;
    // What common styles has been used in prev. specified entry
    cssUsedPaths?: string[];

    // Custom header. Set null for disabling that
    header?: null | (() => Promise<typeof import('*.vue')>);
    // Custom sidebar. Set null for disabling that
    sidebar?: null | (() => Promise<typeof import('*.vue')>);
  };
}

export const routes: Route[] = [
  // ----------------------------- //
  // ----------- Front ----------- //
  // ----------------------------- //

  {
    path: '/develop',
    name: 'develop',
    component: 'DevelopPage',

    meta: {
      title: 'Develop',

      sidebar: null
    }
  },

  {
    path: '/',
    name: 'home',
    component: 'HomePage',

    meta: {
      auth: true,
      isHome: true,
      title: 'Головна',

      sidebar: null
    }
  },

  {
    path: '/reporting',
    name: 'reporting-general',
    component: 'reporting/AdminReportingGeneralPage',

    meta: {
      auth: true,
      title: 'Звітність зведена',
      sidebar: null,

      header: () => import('@/components/common/TheHeader.vue')
    }
  },

  {
    path: '/abonents',
    name: 'abonents-all',
    component: 'abonents/AbonentsListAllPage',

    meta: {
      auth: true,
      title: 'Абоненти',
      sidebar: null,

      header: () => import('@/components/common/TheHeader.vue')
    }
  },

  {
    path: '/abonent/:id',
    name: 'abonent',
    component: 'abonents/AbonentItemPage',

    meta: {
      auth: true,
      title: ' Абонент',
      sidebar: null,

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/mertvi/:village',
    name: 'mertvi',
    component: 'abonents/AbonentsListDisabledPage',

    meta: {
      auth: true,
      title: ' Відключені',
      sidebar: null,

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/selo/:village',
    name: 'villagers',
    component: 'abonents/AbonentsListVillagersPage',

    meta: {
      auth: true,
      title: 'Абоненти',
      sidebar: null,

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/privileged/:village',
    name: 'privileged',
    component: 'abonents/AbonentsListPrivilegedPage',

    meta: {
      auth: true,
      title: ' Пільговики',
      sidebar: null,

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/pilg/:village',
    name: 'privileged-water',
    component: 'water/WaterPrivilegedPage',

    meta: {
      auth: true,
      title: ' Пільговики',
      sidebar: null,

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/subsid/:village',
    name: 'subsidies-water',
    component: 'water/WaterSubsidiesPage',

    meta: {
      auth: true,
      title: ' Пільговики',
      sidebar: null,

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/residents/:village',
    name: 'residents',
    component: 'abonents/AbonentsListResidentsPage',

    meta: {
      auth: true,
      title: ' Дачники',
      sidebar: null,

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/bill/:village',
    name: 'bill',
    component: 'abonents/AbonentsListBillPage',

    meta: {
      auth: true,
      title: 'Рахунки',
      sidebar: null,

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/bill-warning/:village',
    name: 'bill-warning',
    component: 'abonents/BillingWarningsPage',

    meta: {
      auth: true,
      title: 'Рахунки-попередження',
      sidebar: null,

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/borg',
    name: 'promisers',
    component: 'abonents/AbonentsListPromisersPage',

    meta: {
      auth: true,
      title: 'Боржники',
      sidebar: null,

      header: () => import('@/components/common/TheHeader.vue')
    }
  },

  {
    path: '/bank/:village',
    name: 'payment-bank',
    component: 'payment/PaymentBankPage',

    meta: {
      auth: true,
      title: 'Банк',
      sidebar: null,

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/cash/:village',
    name: 'payment-cash',
    component: 'payment/PaymentCashPage',

    meta: {
      auth: true,
      title: 'Готівка',
      sidebar: null,

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/stat/:village',
    name: 'reporting-transaction',
    component: 'reporting/ReportingTransactionPage',

    meta: {
      auth: true,
      title: 'Статистика',
      sidebar: null,

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/pererah/:village',
    name: 'reporting-recalculation',
    component: 'reporting/ReportingRecalculationPage',

    meta: {
      auth: true,
      title: 'Статистика',
      sidebar: null,

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  // ------------------------------------ //
  // --------------- Auth --------------- //
  // ------------------------------------ //

  {
    path: '/auth',
    name: 'auth',
    component: 'admin/AdminAuthPage',

    meta: {
      auth: false,
      title: 'Увійти',

      sidebar: null
    }
  },

  // ------------------------------------- //
  // --------------- Admin --------------- //
  // ------------------------------------- //

  {
    path: '/admin',
    name: 'admin-home',
    component: 'admin/AdminHomePage',

    meta: {
      auth: true,
      title: 'Admin - Головна',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/abonents',
    name: 'admin-abonents',
    component: 'admin/abonents/AdminAbonentsListPage',

    redirect: {
      name: 'admin-abonents-tab',
      params: {
        tab: 'list'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Абоненти',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/abonents/:tab',
    name: 'admin-abonents-tab',
    component: 'admin/abonents/AdminAbonentsListPage',

    meta: {
      auth: true,
      title: 'Admin - Абоненти',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/abonent/:id',
    name: 'admin-abonent',
    component: 'admin/abonents/AdminAbonentItemPage',

    redirect: {
      name: 'admin-abonent-tab',
      params: {
        tab: 'info'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Абонент',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/abonent/:id/:tab',
    name: 'admin-abonent-tab',
    component: 'admin/abonents/AdminAbonentItemPage',

    meta: {
      auth: true,
      title: 'Admin - Абонент',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/payment-types',
    name: 'admin-payment-types',
    component: 'admin/payment-types/AdminPaymentTypesListPage',

    redirect: {
      name: 'admin-payment-types-tab',
      params: {
        tab: 'list'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Види оплат',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/payment-types/:tab',
    name: 'admin-payment-types-tab',
    component: 'admin/payment-types/AdminPaymentTypesListPage',

    meta: {
      auth: true,
      title: 'Admin - Види оплат',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/payment-type/:id',
    name: 'admin-payment-type',
    component: 'admin/payment-types/AdminPaymentTypeItemPage',

    redirect: {
      name: 'admin-payment-type-tab',
      params: {
        tab: 'info'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Вид оплати',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/payment-type/:id/:tab',
    name: 'admin-payment-type-tab',
    component: 'admin/payment-types/AdminPaymentTypeItemPage',

    meta: {
      auth: true,
      title: 'Admin - Вид оплати',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/privileges',
    name: 'admin-privileges',
    component: 'admin/privileges/AdminPrivilegesListPage',

    redirect: {
      name: 'admin-privileges-tab',
      params: {
        tab: 'list'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Пільги',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/privileges/:tab',
    name: 'admin-privileges-tab',
    component: 'admin/privileges/AdminPrivilegesListPage',

    meta: {
      auth: true,
      title: 'Admin - Пільги',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/privilege/:id',
    name: 'admin-privilege',
    component: 'admin/privileges/AdminPrivilegeItemPage',

    redirect: {
      name: 'admin-privilege-tab',
      params: {
        tab: 'info'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Вид пiльги',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/privilege/:id/:tab',
    name: 'admin-privilege-tab',
    component: 'admin/privileges/AdminPrivilegeItemPage',

    meta: {
      auth: true,
      title: 'Admin - Вид пiльги',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/villages',
    name: 'admin-villages',
    component: 'admin/villages/AdminVillagesListPage',

    redirect: {
      name: 'admin-villages-tab',
      params: {
        tab: 'list'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Села',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/villages/:tab',
    name: 'admin-villages-tab',
    component: 'admin/villages/AdminVillagesListPage',

    meta: {
      auth: true,
      title: 'Admin - Села',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/village/:id',
    name: 'admin-village',
    component: 'admin/villages/AdminVillageItemPage',

    redirect: {
      name: 'admin-village-tab',
      params: {
        tab: 'info'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Село',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/village/:id/:tab',
    name: 'admin-village-tab',
    component: 'admin/villages/AdminVillageItemPage',

    meta: {
      auth: true,
      title: 'Admin - Село',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/record-types',
    name: 'admin-record-types',
    component: 'admin/record-types/AdminRecordTypesListPage',

    redirect: {
      name: 'admin-record-types-tab',
      params: {
        tab: 'list'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Типи обліку',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/record-types/:tab',
    name: 'admin-record-types-tab',
    component: 'admin/record-types/AdminRecordTypesListPage',

    meta: {
      auth: true,
      title: 'Admin - Типи обліку',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/record-type/:id',
    name: 'admin-record-type',
    component: 'admin/record-types/AdminRecordTypeItemPage',

    redirect: {
      name: 'admin-record-type-tab',
      params: {
        tab: 'info'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Тип обліку',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/record-type/:id/:tab',
    name: 'admin-record-type-tab',
    component: 'admin/record-types/AdminRecordTypeItemPage',

    meta: {
      auth: true,
      title: 'Admin - Тип обліку',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/transactions',
    name: 'admin-transactions',
    component: 'admin/transactions/AdminTransactionsListPage',

    redirect: {
      name: 'admin-transactions-tab',
      params: {
        tab: 'list'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Транзакції',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/transactions/:tab',
    name: 'admin-transactions-tab',
    component: 'admin/transactions/AdminTransactionsListPage',

    meta: {
      auth: true,
      title: 'Admin - Транзакції',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/transaction/:id',
    name: 'admin-transaction',
    component: 'admin/transactions/AdminTransactionItemPage',

    redirect: {
      name: 'admin-transaction-tab',
      params: {
        tab: 'info'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Транзакція',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/transaction/:id/:tab',
    name: 'admin-transaction-tab',
    component: 'admin/transactions/AdminTransactionItemPage',

    meta: {
      auth: true,
      title: 'Admin - Транзакція',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/users-legal',
    name: 'admin-users-legal',
    component: 'admin/users-legal/AdminUsersLegalListPage',

    redirect: {
      name: 'admin-users-legal-tab',
      params: {
        tab: 'list'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Юридичнi особи',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/users-legal/:tab',
    name: 'admin-users-legal-tab',
    component: 'admin/users-legal/AdminUsersLegalListPage',

    meta: {
      auth: true,
      title: 'Admin - Юридичнi особи',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/user-legal/:id',
    name: 'admin-user-legal',
    component: 'admin/users-legal/AdminUserLegalItemPage',

    redirect: {
      name: 'admin-user-legal-tab',
      params: {
        tab: 'info'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Юридична особа',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/user-legal/:id/:tab',
    name: 'admin-user-legal-tab',
    component: 'admin/users-legal/AdminUserLegalItemPage',

    meta: {
      auth: true,
      title: 'Admin - Юридична особа',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/companies',
    name: 'admin-companies',
    component: 'admin/companies/AdminCompaniesListPage',

    redirect: {
      name: 'admin-companies-tab',
      params: {
        tab: 'list'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Господарства',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/companies/:tab',
    name: 'admin-companies-tab',
    component: 'admin/companies/AdminCompaniesListPage',

    meta: {
      auth: true,
      title: 'Admin - Господарства',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/company/:id',
    name: 'admin-company',
    component: 'admin/companies/AdminCompanyItemPage',

    redirect: {
      name: 'admin-company-tab',
      params: {
        tab: 'list'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Господарство',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/company/:id/:tab',
    name: 'admin-company-tab',
    component: 'admin/companies/AdminCompanyItemPage',

    meta: {
      auth: true,
      title: 'Admin - Господарство',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/users',
    name: 'admin-users',
    component: 'admin/users/AdminUsersListPage',

    redirect: {
      name: 'admin-users-tab',
      params: {
        tab: 'list'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Користувачі',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/users/:tab',
    name: 'admin-users-tab',
    component: 'admin/users/AdminUsersListPage',

    meta: {
      auth: true,
      title: 'Admin - Користувачі',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/user/:id',
    name: 'admin-user',
    component: 'admin/users/AdminUserItemPage',

    redirect: {
      name: 'admin-user-tab',
      params: {
        tab: 'info'
      }
    },

    meta: {
      auth: true,
      title: 'Admin - Користувач',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  {
    path: '/admin/user/:id/:tab',
    name: 'admin-user-tab',
    component: 'admin/users/AdminUserItemPage',

    meta: {
      auth: true,
      title: 'Admin - Користувач',

      header: () => import('@/components/common/TheAdminHeader.vue')
    }
  },

  // ------------------------------------ //
  // -------------- Others -------------- //
  // ------------------------------------ //

  {
    path: '/billing/chart',
    name: 'billing-chart',
    component: 'billing/BillingChartsPage',

    meta: {
      auth: true,
      title: 'Графiк',
      sidebar: null,

      header: () => import('@/components/common/TheAbonentHeader.vue')
    }
  },

  {
    path: '*',
    name: 'error-404',
    component: 'Error404Page',

    meta: {
      title: 'Сторінку не знайдено'
    }
  }
];

export default routes;
