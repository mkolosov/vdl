/* eslint-disable max-classes-per-file */

export enum EventStates {
  LISTENING,
  FINISHED
}

export enum EventTypes {
  ONCE,
  LOOP
}

export class Event {
  private readonly callback: (...args) => void | null = null;

  state = EventStates.LISTENING;
  type = EventTypes.ONCE;

  constructor(callback: (...args) => void, type = EventTypes.ONCE) {
    this.callback = callback;
    this.type = type;
  }

  /**
   * Call event with data
   * @param data Some object
   */
  call(data: any[]) {
    if (this.type === EventTypes.ONCE) {
      this.end(data);
      return;
    }

    this.callback(...data);
  }

  /**
   * Call event with data finally
   * @param data Some object
   */
  end(data: any[]) {
    if (this.state === EventStates.FINISHED) return;

    this.state = EventStates.FINISHED;
    this.callback(...data);
  }

  /**
   * Set type of calling event
   * @param type ONCE - will work once, LOOP - continuous operation
   */
  setType(type: EventTypes) {
    this.type = type;
  }
}

/**
 * A class that allows you to create an event bus.
 *
 * It is possible to subscribe a function to a certain event by a given name,
 * which can be called from any other place and at any time
 * by the above name, if the subscription is still active at that moment
 */
export class EventBus {
  private static listeners = new Map<string, Event[]>();

  /**
   * Subscribe function to event by given name
   * @param key Name of event
   * @param callback Calling function
   * @param type ONCE - will work once, LOOP - continuous operation
   * @returns Event object
   */
  static on(key: string, callback: (...args) => void, type = EventTypes.ONCE) {
    const evt = new Event(callback, type);

    if (!EventBus.listeners.has(key)) EventBus.listeners.set(key, []);
    EventBus.listeners.get(key).push(evt);

    return evt;
  }

  /**
   * Unsubscribe function from event by given name
   * @param key Name of event
   * @param evt Object of a previously signed event
   */
  static off(key: string, evt: Event) {
    if (EventBus.listeners.has(key)) {
      const index = EventBus.listeners.get(key).indexOf(evt);
      EventBus.listeners.get(key).splice(index, 1);

      if (!EventBus.listeners.get(key).length) {
        EventBus.listeners.delete(key);
      }
    }
  }

  /**
   * Call all functions subscribed to an event
   * @param key Name of event
   * @param data Data passed to called functions
   */
  static dispatch(key, ...data) {
    if (EventBus.listeners.has(key)) {
      EventBus.listeners.get(key).forEach((evt) => evt.call(data));
    }
  }
}
