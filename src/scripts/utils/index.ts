import UConst from '@/scripts/utils/modules/Consts';
import UNumber from '@/scripts/utils/modules/Number';
import UString from '@/scripts/utils/modules/String';
import UDocument from '@/scripts/utils/modules/Document';
import UConsole from '@/scripts/utils/modules/Console';
import UImage from '@/scripts/utils/modules/Image';
import UGeo from '@/scripts/utils/modules/Geo';

export default class Utils {
  public static Const = UConst;
  public static Number = UNumber;
  public static String = UString;
  public static Document = UDocument;
  public static Console = UConsole;
  public static Image = UImage;
  public static Geo = UGeo;
}
