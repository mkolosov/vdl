/* eslint-disable max-classes-per-file */

export const isDev = process.env.NODE_ENV === 'development';
export const isDebug = !!JSON.parse(localStorage.getItem('debug'));

export interface GA {
  non_interaction?: boolean;
  event_category?: string;
  event_label?: string;
  value?: string;
}

export enum Colors {
  app = '#fccc6b',
  timelapse = '#b455ff',
  module = '#ff6363',
  router = '#42b983',
  icon = '#43d9d8',
  gtag = '#ff7336',
  css = '#33a9dc'
}

export class UConsoleMessage {
  private tags: Array<{
    name: string;
    color: string;
  }> = [];

  /**
   * Add colored section like [NAME]
   * @param name Name of section
   * @param color Color of section
   */
  addTag(name: string, color?: string) {
    this.tags.push({
      name,
      color: color || Colors[name.toLowerCase()] || '#ffffff'
    });

    return this;
  }

  /**
   * Add several sections with predefined colors
   * @param tags
   */
  addTags(...tags) {
    tags.forEach((tag) => this.addTag(tag));
    return this;
  }

  /**
   * Build sections in one console.log-array
   */
  build() {
    const sections = [];
    const colors = [];

    this.tags.forEach((tag) => {
      sections.push(`%c[${tag.name}]`);
      colors.push(`color: ${tag.color};`);
    });

    sections.push('%c');
    colors.push('');

    return [`${sections.join('')}`].concat(colors);
  }

  /**
   * Finish building and out result to browser console
   * @param messages
   */
  end(...messages) {
    if (!isDev && !isDebug) return;

    const prefix = this.build();
    console.log(...prefix, ...messages);
  }
}

export default class UConsole {
  /**
   * Create console message with sections possibility
   */
  static create() {
    return new UConsoleMessage();
  }

  /**
   * Print out timelapse for app
   * @param args
   */
  public static timelapse(...args) {
    UConsole.create()
      .addTags('APP', 'Timelapse')
      .end(args[0], Date.now() - window.timelapseStart, ...args.slice(1));
  }

  /**
   * Print out timelapse for module
   * @param args
   */
  public static timelapseModule(...args) {
    UConsole.create()
      .addTags('APP', 'Timelapse', 'Module')
      .end(...args);
  }

  /**
   * Print out GoogleAnalytics event
   * @param name Name of event
   * @param data Sent data
   */
  public static ga(name, data: GA = {}) {
    // @ts-ignore
    if (window.gtag) {
      UConsole.create()
        .addTags('APP', 'GTAG')
        .end(`Sent "${name}"`, data);

      gtag('event', name, data);
    }
  }
}
