import UNumber from '@/scripts/utils/modules/Number';

export default class UString {
  /**
   * Generate url-query string from object
   * @param obj Origin object
   */
  static serialize(obj): string {
    if (!obj) return '';
    const str = Object.keys(obj).map((p) => `${encodeURIComponent(p)}=${encodeURIComponent(obj[p])}`);
    return `?${str.join('&')}`;
  }

  /**
   * Reverse chars in string
   * @param str Origin string
   */
  static reverse(str: string) {
    return String(str).split('').reverse().join('');
  }

  /**
   * Change case of first char to upper
   * @param str
   */
  static ucfirst(str = '') {
    return String(str).charAt(0).toUpperCase() + String(str).substr(1, String(str).length - 1);
  }

  /**
   * Returns string with right suffix depends of count
   * @param one If count ends with 1
   * @param three If count ends with 2-4
   * @param many If count ends with 5-0
   * @param count
   */
  static suffix(one, three, many, count) {
    if (count < 10) {
      if (count === 1) return one;
      if (count > 1 && count < 5) return three;

      return many;
    }

    return UString.suffix(one, three, many, count % 10);
  }

  /**
   * Generate random hash
   * @param len Length of string
   */
  static hash(len = 32) {
    let hash = '';
    for (let i = 0; i < len; i += 1) {
      const chance = UNumber.rand(0, 2);
      if (chance === 0) {
        hash += UNumber.rand(0, 9);
      } else {
        const upperCaseRange = UNumber.rand(65, 90);
        const lowerCaseRange = UNumber.rand(97, 122);
        hash += String.fromCharCode(chance === 1 ? upperCaseRange : lowerCaseRange);
      }
    }

    return hash;
  }

  /**
   * Convert date to "31.12.9999 23:59" format
   * @param date {Date} Origin date
   */
  static toDateTime(date = new Date()) {
    return `${UString.toDate(date)} ${UString.toTime(date)}`;
  }

  /**
   * Convert date to "31.12.9999" format
   * @param date {Date} Origin date
   */
  static toDate(date = new Date()) {
    return date.toLocaleDateString().split('-').join('.');
  }

  /**
   * Convert date to "31.12" format
   * @param date {Date} Origin date
   */
  static toShortDate(date = new Date()) {
    return UString.toDate(date).substr(0, 5);
  }

  /**
   * Convert date to "23:59" format
   * @param date {Date} Origin date
   */
  static toTime(date = new Date()) {
    return date.toLocaleTimeString().substr(0, 5);
  }

  /**
   * Convert number to "999.999.99" format
   * @param num Origin number
   * @param delimiter Delimiter between hundreds
   */
  static toMoney(num, delimiter = '.') {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, `$1${delimiter}`);
  }

  /**
   * Convert number to "+00 000 000 00 00"
   * @param str
   * @param d
   */
  static toPhone(str, d = ' ') {
    const text = UString.reverse(UString.reverse(str).replace(/(\d{2})(\d{2})(\d{3})(\d{3})(\d+?)/, `$1${d}$2${d}$3${d}$4${d}$5`));
    return `+${text}`;
  }

  /**
   * Composition of a row with a unit of size, if the last is missing
   * @param value Css-property
   * @param units Unit size
   */
  static cssPropFit(value, units = 'px') {
    if (value === 'auto') return value;

    const textValue = String(Math.round(value) || value);
    const ifUnitNotSpecified = parseInt(value, 10).toString().length === textValue.length;
    return ifUnitNotSpecified ? `${value}${units}` : value;
  }

  /**
   * Composition of a string with color, checking for the presence of variables
   * @param color Color
   */
  static cssColorChain(color) {
    if ((color || '')[0] === '#') return color;
    return `var(--btn-${color}, var(--vd-${color}, var(--${color}, ${color})))`;
  }
}
