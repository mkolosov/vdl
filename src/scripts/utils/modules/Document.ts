import vuex from '@/scripts/store';
import UString from '@/scripts/utils/modules/String';
import { VDError } from '@/scripts/store/modules/Global';

export default class UDocument {
  /**
   * Change class of body element
   * @param flag State of loading
   */
  static setLoading(flag) {
    document.body.classList[flag ? 'remove' : 'add']('loaded');
  }

  /**
   * Set title of document
   * @param route Vue-router item
   */
  static setDocumentTitle(route) {
    if (!route) return;

    const prefix = process.env.VUE_APP_TITLE;
    let title = route.meta.title || route.name;
    if (route.params.id) title = `${title} ${route.params.id}`;

    document.title = `${title} | ${UString.ucfirst(prefix)}`;
  }

  /**
   * Open share-modal on phone-browsers
   * @param text
   * @param title
   * @param url
   */
  static share(text = '', title = document.title, url = window.location.href) {
    // @ts-ignore
    if (window.navigator.share) {
      // @ts-ignore
      window.navigator.share({ url, text, title })
        .catch((err) => {
          console.error('Error sharing:', err);
        });
    }
  }

  /**
   * Open TheErrorModal
   * @param error
   */
  static async throw(error: VDError) {
    await vuex.commit('Global/setError', error);
  }
}
